#include <stdio.h>			/* printf */
#include <semaphore.h>		/* sem_t, sem_init, sem_wait, sem_post */
#include <pthread.h>		/* pthread_t, pthread_create, pthread_join */
#include <unistd.h>			/* sleep */

#define N 5
#define LEFT (i + N - 1) % N
#define RIGHT (i + 1) % N
#define THINKING 0
#define HUNGRY 1
#define EATING 2
#define ANTALL 50

sem_t mutex;
sem_t s[N];
int antall = 0;

int state[N]; // Status på filosof
int philosophers[N] = { 0, 1, 2, 3, 4 }; // Navn/nummer på filosofer

void * philospher(void *num); 
void take_forks(int);
void put_forks(int);
void test(int);

void think(int);
void eat(int);

int main() {
	int i;
	pthread_t thread_id[N];
	sem_init(&mutex, 0, 1);
	
	for (i = 0; i < N; i++) {
		sem_init(&s[i], 0, 0);
	}
	
	for (i = 0; i < N; i++) {
		pthread_create(&thread_id[i], NULL, philospher, &philosophers[i]);
		printf("Filosof %d tenker!\n", i + 1);
	}
	
	for (i = 0; i < N; i++) {
		pthread_join(thread_id[i], NULL);
	}
}

void * philospher(void *num) {
	while (antall < ANTALL) {
		int *i = num;
		think(*i);
		take_forks(*i);
		eat(*i);
		put_forks(*i);
	}

return 0;
}

void take_forks(int i) {
	sem_wait(&mutex);
	state[i] = HUNGRY;
	test(i);
	sem_post(&mutex);
	sem_wait(&s[i]);
}

void put_forks(int i) {
	sem_wait(&mutex);
	state[i] = THINKING;
	test(LEFT);
	test(RIGHT);
	sem_post(&mutex);
}

void test(int i) {
	if (state[i] == HUNGRY && state[LEFT] != EATING && state[RIGHT] != EATING) {
		state[i] = EATING;
		sem_post(&s[i]);
	}
}

void think(int i) {
	printf("Filosof %d tenker!\n", i + 1);
	sleep(1);
}

void eat(int i) {
	printf("\tFilosof %d spiser!\n", i + 1);
	sleep(0);
	antall++;
}
