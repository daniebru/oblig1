#include <unistd.h> //fork
#include <sys/wait.h> //waitpid
#include <stdio.h> //printf
#include <stdlib.h> // exit

// TING OG TANG

// while(wait(NULL) > 0) { } parent venter på alle child prosesser
// exit(0) avslutter prosess med id 0
// waitpid(pid, NULL, 0)
// FUNKSJONER

void prosess(int pro, int num){
printf("Process %d kjører\n",pro);
sleep(num);
printf("\tProsess %d kjørte i %d sekunder\n", pro, num); 
}

// MAIN

int main(void) {

int p;

p = fork();

if(p==0) {
	prosess(2,3); // Prosess 2
	exit(0);
	}

else {
	p=fork();
	if(p==0) { 	// Prosess 0
	 prosess(0,1);
	 exit(0);}
	}

waitpid(p, NULL, 0); // Venter prosess 0


p=fork();
if(p==0) {
	prosess	(4,3);	// Prosess 4
	p=fork();	// Forker childprocess for å starte prosess 5
	if(p==0) { prosess(5,3); exit(0); } // Prosess 5
	waitpid(p,NULL,0);
	exit(0);
	}

else 	p=fork();		// Prosess 1
	if(p==0)  {
	prosess(1,2);
	exit(0); 
	}

waitpid(p,NULL,0); // Venter på prosess 1

p=fork();
if(p==0) { // Prosess 3
	prosess(3,2);
	exit(0);
	}


while(wait(NULL) > 0) { } // Venter på child til prosess 4, altså prosess 5

printf("\nProgram ferdig!\n");

return 0;
}
