# Oblig1

### Deltagere:
| Navn                | Studie | Studentnr. |
| --------------------|:------:|-----------:|
| Daniel Bruun        | BITSEC | 473117     |
| Svein Are Danielsen | BIDAT  | 253067     |
| Johan Aanesen       | BPROG | 473182     |

### Verkt�y brukt:
* GCC med -Wall (Warnings all).

### Kvalitetssikring:
* printf() for debugging under koding.
* Brukt kode fra [repo](https://github.com/NTNUcourses/opsys)
